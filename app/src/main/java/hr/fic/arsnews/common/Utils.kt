package hr.fic.arsnews.common

import android.support.v7.widget.SearchView
import android.view.View
import java.util.*

private const val SECOND_MILLIS = 1000
private const val MINUTE_MILLIS = 60 * SECOND_MILLIS
private const val HOUR_MILLIS = 60 * MINUTE_MILLIS
private const val DAY_MILLIS = 24 * HOUR_MILLIS

fun Date.getTimeAgo(): String {
    val now = System.currentTimeMillis()
    if (time > now || time <= 0) {
        return "in the future"
    }
    val diff = now - time
    return when {
        diff < MINUTE_MILLIS -> "moments ago"
        diff < 2 * MINUTE_MILLIS -> "a minute ago"
        diff < 60 * MINUTE_MILLIS -> (diff / MINUTE_MILLIS).toString() + " minutes ago"
        diff < 2 * HOUR_MILLIS -> "an hour ago"
        diff < 24 * HOUR_MILLIS -> (diff / HOUR_MILLIS).toString() + " hours ago"
        diff < 48 * HOUR_MILLIS -> "yesterday"
        else -> (diff / DAY_MILLIS).toString() + " days ago"
    }
}

inline var View.isVisible: Boolean
    get() = visibility == View.VISIBLE
    set(value) {
        visibility = if (value) View.VISIBLE else View.GONE
    }

inline fun SearchView.onTextChanged(crossinline action: (newText: String) -> Unit) {
    setOnQueryTextListener(object : SearchView.OnQueryTextListener {
        override fun onQueryTextSubmit(query: String?): Boolean = true

        override fun onQueryTextChange(newText: String): Boolean {
            action(newText)
            return true
        }
    })
}