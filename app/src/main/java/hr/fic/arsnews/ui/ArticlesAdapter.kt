package hr.fic.arsnews.ui

import android.content.res.Resources
import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.support.v4.content.res.ResourcesCompat
import android.support.v7.widget.RecyclerView
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.style.ForegroundColorSpan
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import hr.fic.arsnews.R
import hr.fic.arsnews.common.getTimeAgo
import hr.fic.arsnews.databinding.ListArticleBinding
import hr.fic.arsnews.databinding.ListTitleBinding
import hr.fic.arsnews.databinding.ListTopHeadlinesBinding
import hr.fic.arsnews.model.Article
import hr.fic.arsnews.model.ListData
import hr.fic.arsnews.model.TitleItem
import hr.fic.arsnews.model.TopHeadlinesItems

private const val TITLE = 1
private const val TOP = 3
private const val ARTICLE = 4

class MainArticlesAdapter(private val items: ArrayList<ListData>) : RecyclerView.Adapter<MainArticlesAdapter.DataBindingHolder>() {

    override fun getItemViewType(position: Int): Int {
        return when(items[position]) {
            is TitleItem -> TITLE
            is TopHeadlinesItems -> TOP
            else -> ARTICLE
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DataBindingHolder {
        val layoutId = when(viewType) {
            TITLE -> R.layout.list_title
            TOP -> R.layout.list_top_headlines
            ARTICLE -> R.layout.list_article
            else -> throw IllegalArgumentException("Unknown type")
        }
        return DataBindingHolder(DataBindingUtil.inflate(LayoutInflater.from(parent.context), layoutId, parent, false))
    }

    override fun onBindViewHolder(holder: DataBindingHolder, position: Int) {
        val dataBinding = holder.viewDataBinding
        val item = items[position]
        when(dataBinding) {
            is ListTitleBinding -> dataBinding.listItemTitle.setText((item as TitleItem).titleRes)
            is ListTopHeadlinesBinding -> dataBinding.listTopRecycler.adapter = ArticlesTopHeadlinesAdapter((item as TopHeadlinesItems).articles)
            is ListArticleBinding -> {
                val article = item as Article
                dataBinding.articleTitle.text = article.title
                dataBinding.articleDescription.text = article.description
                dataBinding.articleSource.setText(getSourceAndTimeText(dataBinding.articleSource.resources, article), TextView.BufferType.SPANNABLE)
            }
        }
    }

    private fun getSourceAndTimeText(resources: Resources, article: Article): SpannableStringBuilder? {
        val agoText = article.publishedAt?.getTimeAgo()
        val sourceText = article.source?.name
        val colorSpan = ForegroundColorSpan(ResourcesCompat.getColor(resources, R.color.accent, null))
        return SpannableStringBuilder()
                .append(agoText, null, 0)
                .append(" by ", null, 0)
                .append(sourceText, colorSpan, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
    }

    override fun getItemCount(): Int = items.size

    fun setItems(newItems: ArrayList<ListData>?) {
        items.clear()
        if(newItems != null) {
            items.addAll(newItems)
        }
        notifyDataSetChanged()
    }

    class DataBindingHolder(val viewDataBinding: ViewDataBinding) : RecyclerView.ViewHolder(viewDataBinding.root)
}