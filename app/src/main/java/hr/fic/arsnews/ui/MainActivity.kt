package hr.fic.arsnews.ui

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.content.res.ResourcesCompat
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import hr.fic.arsnews.R
import hr.fic.arsnews.common.isVisible
import hr.fic.arsnews.common.onTextChanged
import hr.fic.arsnews.databinding.ActivityMainBinding
import hr.fic.arsnews.model.ArticleViewModel
import hr.fic.arsnews.model.ListData

class MainActivity : AppCompatActivity() {

    lateinit var observer: Observer<ArrayList<ListData>>
    lateinit var binding: ActivityMainBinding
    lateinit var viewModel: ArticleViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        viewModel = ViewModelProviders.of(this).get(ArticleViewModel::class.java)

        binding.mainSwipeRefresh.setColorSchemeColors(ResourcesCompat.getColor(resources, R.color.accent, theme))
        binding.mainSwipeRefresh.setOnRefreshListener {
            viewModel.refreshData()
        }

        viewModel.loadingLiveData.observe(this, Observer {
            binding.mainSwipeRefresh.isRefreshing = it ?: false
        })

        val adapter = MainArticlesAdapter(ArrayList())
        binding.mainRecyclerView.adapter = adapter

        observer = Observer {
            adapter.setItems(it)
            if(it == null) {
                Toast.makeText(this, getString(R.string.error_no_connection), Toast.LENGTH_LONG).show()
            } else {
                binding.mainEmptyView.isVisible = it.isEmpty()
            }
        }

        binding.mainSearchView.onTextChanged {
            adjustRecyclerView(it)
            viewModel.onQueryChange(it)
        }
        adjustRecyclerView(viewModel.query)
    }

    private fun adjustRecyclerView(query: String) {
        binding.mainRecyclerView.clearOnScrollListeners()
        if(query.isNotBlank()) {
            binding.mainRecyclerView.addOnScrollListener(viewModel.pagingScrollListener)
            viewModel.searchResultLiveData.observe(this, observer)
        } else {
            viewModel.getMainData().observe(this, observer)
        }
    }
}