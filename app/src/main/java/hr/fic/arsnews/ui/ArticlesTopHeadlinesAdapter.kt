package hr.fic.arsnews.ui

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.squareup.picasso.Picasso
import hr.fic.arsnews.R
import hr.fic.arsnews.common.getTimeAgo
import hr.fic.arsnews.databinding.ListTopArticleBinding
import hr.fic.arsnews.model.Article

class ArticlesTopHeadlinesAdapter(private val items: ArrayList<Article>) : RecyclerView.Adapter<ArticlesTopHeadlinesAdapter.DataBindingHolder>() {

    override fun getItemCount(): Int = items.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DataBindingHolder {
        return DataBindingHolder(DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.list_top_article, parent, false))
    }

    override fun onBindViewHolder(holder: DataBindingHolder, position: Int) {
        holder.bind(items[position])
    }

    class DataBindingHolder(private val binding: ListTopArticleBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(article: Article) {
            binding.articleTitle.text = article.title
            binding.articleSource.text = article.source?.name
            binding.articleImage.clipToOutline = true
            Picasso.with(binding.articleImage.context).load(article.urlToImage).into(binding.articleImage)
            binding.articleTime.text = article.publishedAt?.getTimeAgo()
        }
    }
}
