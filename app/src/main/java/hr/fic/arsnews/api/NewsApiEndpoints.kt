package hr.fic.arsnews.api

import kotlinx.coroutines.experimental.Deferred
import retrofit2.http.GET
import retrofit2.http.Query

interface NewsApiEndpoints {

    @GET("top-headlines")
    fun getTopHeadlines(
            @Query("sources") sources: String? = null,
            @Query("country") country: String? = null,
            @Query("pageSize") pageSize: Int? = null,
            @Query("page") page: Int? = null,
            @Query("apiKey") apiKey: String
    ): Deferred<ApiResponse>?

    @GET("everything")
    fun getEverything(
            @Query("q") query: String,
            @Query("pageSize") pageSize: Int,
            @Query("page") page: Int,
            @Query("apiKey") apiKey: String
    ): Deferred<ApiResponse>?
}