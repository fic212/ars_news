package hr.fic.arsnews.api

import hr.fic.arsnews.model.Article

data class ApiResponse(
        private val status: String? = null,
        val totalResults: Int? = null,
        val articles: ArrayList<Article>? = null
) {
    fun isSuccess() = status == "ok" && articles != null
}