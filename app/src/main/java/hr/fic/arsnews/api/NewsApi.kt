package hr.fic.arsnews.api

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.experimental.CoroutineCallAdapterFactory
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

private const val BASE_URL = "https://newsapi.org/v2/"
private const val KEY = "044967052498465fa70ea43e6af67034"
private const val DEFAULT_PAGE_SIZE = 10

object NewsApi {

    private val newsApiEndpoints: NewsApiEndpoints

    init {
        val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .build()

        newsApiEndpoints = retrofit.create(NewsApiEndpoints::class.java)
    }

    fun getTopHeadlinesFromBBC() =
            newsApiEndpoints.getTopHeadlines(sources = "bbc-news", pageSize = DEFAULT_PAGE_SIZE, page = 1, apiKey = KEY)

    fun getTopHeadlinesFromUS() =
            newsApiEndpoints.getTopHeadlines(country = "us", apiKey = KEY)

    fun getEverythingForQuery(query: String, page: Int) =
            newsApiEndpoints.getEverything(query = query, page = page, pageSize = DEFAULT_PAGE_SIZE, apiKey = KEY)
}