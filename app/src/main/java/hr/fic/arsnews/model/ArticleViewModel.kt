package hr.fic.arsnews.model

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.support.v7.widget.RecyclerView
import android.util.Log
import hr.fic.arsnews.R
import hr.fic.arsnews.api.ApiResponse
import hr.fic.arsnews.api.NewsApi
import hr.fic.arsnews.common.PagingScrollListener
import kotlinx.coroutines.experimental.Deferred
import kotlinx.coroutines.experimental.async
import java.net.ConnectException

class ArticleViewModel : ViewModel() {

    val loadingLiveData = MutableLiveData<Boolean>()

    private var mainLiveData: MutableLiveData<ArrayList<ListData>>? = null

    private var callForQuery: Deferred<ApiResponse>? = null
    var searchResultLiveData = MutableLiveData<ArrayList<ListData>>()
    var query: String = ""
    val pagingScrollListener: PagingScrollListener = object : PagingScrollListener() {
        override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView?) {
            getDataForQuery(page)
        }
    }

    fun getMainData(force: Boolean = false): MutableLiveData<ArrayList<ListData>> {
        if(mainLiveData == null || force) {
            if(mainLiveData == null) {
                mainLiveData = MutableLiveData()
            }
            async {
                loadingLiveData.postValue(true)
                val mainListData = ArrayList<ListData>()
                val topHeadlinesResponse = try {
                    NewsApi.getTopHeadlinesFromBBC()?.await()
                } catch(e: Exception) {
                    handleException(mainLiveData!!, e)
                    return@async null
                }
                if(topHeadlinesResponse?.isSuccess() == true) {
                    mainListData.add(TitleItem(R.string.title_top_headlines))
                    mainListData.add(TopHeadlinesItems(topHeadlinesResponse.articles!!))
                }
                val articlesResponse = try {
                    NewsApi.getTopHeadlinesFromUS()?.await()
                } catch(e: Exception) {
                    handleException(mainLiveData!!, e)
                    return@async null
                }

                if(articlesResponse?.isSuccess() == true) {
                    mainListData.add(TitleItem(R.string.title_latest_news))
                    mainListData.addAll(articlesResponse.articles!!)
                }
                loadingLiveData.postValue(false)
                mainLiveData!!.postValue(mainListData)
            }
        }
        return mainLiveData!!
    }

    fun onQueryChange(newQuery: String, force: Boolean = false) {
        if(query != newQuery || force) {
            query = newQuery
            pagingScrollListener.resetState()
            searchResultLiveData.value?.clear()
            if(query.isNotBlank()) {
                getDataForQuery(pagingScrollListener.currentPage)
            } else {
                callForQuery?.cancel()
                loadingLiveData.postValue(false)
                mainLiveData?.postValue(mainLiveData?.value)
            }
        }
    }

    fun getDataForQuery(page: Int) = async {
        if(pagingScrollListener.currentPage - 1 < page) {
            loadingLiveData.postValue(true)
            callForQuery?.cancel()
            callForQuery = NewsApi.getEverythingForQuery(query, page)
            val response = try {
                callForQuery?.await()
            } catch(e: Exception) {
                handleException(searchResultLiveData, e)
                null
            }
            if(response?.isSuccess() == true) {
                var list = searchResultLiveData.value
                if(list == null) {
                    list = ArrayList()
                    list.add(TitleItem(R.string.title_search_results))
                }
                list.addAll(response.articles!!)
                searchResultLiveData.postValue(list)
            }
            loadingLiveData.postValue(false)
        }
    }

    fun refreshData() {
        if(query.isNotBlank()) {
            onQueryChange(query, true)
        } else {
            getMainData(true)
        }
    }

    private fun handleException(liveData: MutableLiveData<ArrayList<ListData>>, e: Exception) {
        if(e is ConnectException) {
            Log.w("ArticleViewModel", "Error on network call", e)
            liveData.postValue(null)
            loadingLiveData.postValue(false)
        }
    }
}