package hr.fic.arsnews.model

import java.util.*

interface ListData

data class Article(
        val source: ArticleSource? = null,
        val author: String? = null,
        val title: String? = null,
        val description: String? = null,
        val url: String? = null,
        val urlToImage: String? = null,
        val publishedAt: Date? = null
) : ListData

class TitleItem(val titleRes: Int) : ListData

class TopHeadlinesItems(val articles: ArrayList<Article>) : ListData

data class ArticleSource(
        val id: String? = null,
        val name: String? = null
)
